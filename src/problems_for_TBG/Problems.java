package problems_for_TBG;

import utils.*;

final public class Problems {

	public static String chukaSavaa(int x) {
		String res = "";

		x = x / 4 - (x % 4 == 0 ? 1 : 0);
		res = String.format("%d", x);
		return res;
	}

	public static String chukaSquares(int size, int[] colors) {
		int min = Utils.findMin(colors);
		int res = min * size;
		int maxCount = 0;
		int firstZeroTarget = -1;
		int index = 0;
		int count = 0;
		int cycleCount = 0;

		while (cycleCount < 2 || index != firstZeroTarget) {
			if (colors[index] - min != 0) {
				count++;
			} else {
				maxCount = Math.max(maxCount, count);
				count = 0;
				if (firstZeroTarget == -1) {
					firstZeroTarget = index;
				}
			}
			index++;

			if (index == size) {
				index = 0;
				cycleCount++;
			}
		}

		return String.format("%d", res + maxCount);
	}

	public static int spiralValue(int size, int x, int y) {
		int layer = Math.min(Math.min(x, y), Math.min(size - 1 - x, size - 1 - y));

		int startValue = 1;
		if (layer > 0) {
			startValue += 4 * layer * (size - layer);
		}

		int layerSize = size - 2 * layer;

		if (x == layer) {
			return startValue + (y - layer); // deed heseg
		} else if (y == size - layer - 1) {
			return startValue + (layerSize - 1) + (x - layer); // baruun heseg
		} else if (x == size - layer - 1) {
			return startValue + 2 * (layerSize - 1) + (size - layer - 1 - y); // dood heseg
		} else {
			return startValue + 3 * (layerSize - 1) + (size - layer - 1 - x); // zuun heseg
		}
	}
}
