package problems_for_TBG;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import utils.*;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class User_interface extends CustomValidation {

	private JFrame frame;
	private JTextField text_res1;
	private JTextField savaaSize;
	private JTextField chukaSquarRes;
	private JTextField size;
	private JTextField spiralMatrixRes;
	private JTextField spiralMatrixSize;
	private JTextField fieldArray;
	private JTextField txtX;
	private JTextField txtY;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					User_interface window = new User_interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public User_interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 621, 392);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 669, 415);
		frame.getContentPane().add(tabbedPane);

		// Problem 1

		JPanel panelChukaSavaa = new JPanel();
		panelChukaSavaa.setName("chukaSavaa");
		tabbedPane.addTab("Чулуунаа ба Саваа", null, panelChukaSavaa, null);
		panelChukaSavaa.setVisible(false);
		panelChukaSavaa.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 167, 235, 145);
		panelChukaSavaa.add(panel);
		panel.setLayout(null);
		panel.setBorder(null);
		panel.setBackground(new Color(255, 218, 185));

		JLabel labelRes1 = new JLabel("Хариу");
		labelRes1.setFont(new Font("Arial", Font.BOLD, 16));
		labelRes1.setBounds(93, 11, 105, 28);
		panel.add(labelRes1);

		text_res1 = new JTextField();
		text_res1.setHorizontalAlignment(SwingConstants.CENTER);
		text_res1.setEditable(false);
		text_res1.setColumns(10);
		text_res1.setBounds(44, 50, 147, 52);
		panel.add(text_res1);

		JLabel lblValidation1 = new JLabel("");
		lblValidation1.setBackground(new Color(255, 255, 255));
		lblValidation1.setHorizontalAlignment(SwingConstants.CENTER);
		lblValidation1.setForeground(new Color(255, 128, 128));
		lblValidation1.setVerifyInputWhenFocusTarget(false);
		lblValidation1.setBounds(10, 56, 235, 34);
		panelChukaSavaa.add(lblValidation1);

		savaaSize = new JTextField();
		savaaSize.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (savaaSize.getText().equals("n")) {
					savaaSize.setText("");
					savaaSize.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (savaaSize.getText().isEmpty()) {
					savaaSize.setForeground(Color.GRAY);
					savaaSize.setText("n");
				}
			}
		});
		savaaSize.setText("n");
		savaaSize.setBounds(10, 11, 235, 34);
		panelChukaSavaa.add(savaaSize);
		savaaSize.setHorizontalAlignment(SwingConstants.CENTER);
		savaaSize.setColumns(10);

		JButton check1 = new JButton("");
		check1.setBounds(10, 97, 235, 59);
		panelChukaSavaa.add(check1);
		check1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!savaaSize.getText().isBlank()) {
					if (isNumeric(savaaSize.getText())) {
						int n = Integer.parseInt(savaaSize.getText());
						if (n % 2 == 0) {
							lblValidation1.setText(null);
							text_res1.setText(Problems.chukaSavaa(Integer.parseInt(savaaSize.getText())));
						} else
							lblValidation1.setText("s!");
					} else
						lblValidation1.setText("Зөвхөн натурал тоо !!!");
				} else {
					lblValidation1.setText("Талбарыг бөгөлнө үү!");
				}

			}
		});
		check1.setIcon(
				new ImageIcon("D:\\apply\\B190910051_week3_problems.zip_expanded\\Problems\\images\\click50px.png"));

		JScrollPane problame1 = new JScrollPane();
		problame1.setBounds(277, 11, 316, 301);
		panelChukaSavaa.add(problame1);

		JTextArea txtrN = new JTextArea();
		txtrN.setLineWrap(true);
		txtrN.setWrapStyleWord(true);
		txtrN.setText(
				"Чулуунаад n эерэг бүхэл тоон урттай модон саваа байна. Тэр савааг дөрвөн хэсэгт хуваахын\r\nтулд яг гурван удаа хэрчинэ. Хэсэг бүр эерэг бүхэл тоон урттай байх ёстой ба эдгээр уртуудын\r\nнийлбэр мэдээж n байна.\r\nЧука тэгш өнцөгтөд дуртай боловч квадратад дургүй. Тэр хуваасан хэсгүүдийг ашиглан тэгш\r\nөнцөгт үүсгэх боломжтой боловч квадрат үүсгэх боломжгүй байхаар савааг дөрвөн хэсэгт\r\nхуваах хэдэн зам байх вэ гэж гайхсан.\r\nТаны ажил бол Чукад туслах ба нийт боломжын тоог тоолох юм. Савааг хуваах дараах хоёр\r\nарга ялгаатайд тооцогдоно: эхний аргаар хуваасан x урттай хэсгүүдийн тоо хоёр дахь аргаар\r\nхуваасан x урттай хэсгүүдийн тооноос ялгаатай байх x бүхэл тоон утга байвал.\r\nОролт:\r\nОролтын эхний мөрөнд эерэг бүхэл тоон утга n (1≤n≤2∗109\r) байх ба энэ нь Чулуунаагын\r\n\r\nсавхны урт байна.\r\nГаралт:\r\nГаралтанд нэг бүхэл тоон утга байх ба хуваасан хэсгүүдийн төгсгөлүүдийг холбон тэгш өнцөгт\r\nүүсгэх боломжтой боловч квадрат үүсгэх боломжгүй байхаар Чулуунаагын савааг эерэг бүхэл\r\nтоон урттай дөрвөн хэсэгт хуваах аргын тоо байна.\r\n\r\nЖишээ тэстүүд:\r\n\r\nОролт: 6\r\nГаралт: 1\r\n\r\nОролт: 20\r\nГаралт: 4\r\n\r\nТэмдэглэл:\r\nЭхний жишээн дээр савааг хуваах ганц арга байна {1, 1, 2, 2}.\r\nХоёр дахь жишээн дээр савааг хуваах дөрвөн арга байна {1, 1, 9, 9}, {2, 2, 8, 8}, {3, 3, 7, 7} ба\r\n{4, 4, 6, 6}. Мөн {5, 5, 5, 5} гэж хувааж болохгүйг сана.");
		problame1.setViewportView(txtrN);

		// Problem 2

		JPanel panel_chukaSquares = new JPanel();
		tabbedPane.addTab("Чулуунаа ба квадратууд", null, panel_chukaSquares, null);
		panel_chukaSquares.setLayout(null);

		JPanel panel_square_res = new JPanel();
		panel_square_res.setLayout(null);
		panel_square_res.setBorder(null);
		panel_square_res.setBackground(new Color(255, 218, 185));
		panel_square_res.setBounds(10, 167, 235, 145);
		panel_chukaSquares.add(panel_square_res);

		JLabel lblRes1 = new JLabel("Хариу");
		lblRes1.setFont(new Font("Arial", Font.BOLD, 16));
		lblRes1.setBounds(93, 11, 105, 28);
		panel_square_res.add(lblRes1);

		chukaSquarRes = new JTextField();
		chukaSquarRes.setHorizontalAlignment(SwingConstants.CENTER);
		chukaSquarRes.setEditable(false);
		chukaSquarRes.setColumns(10);
		chukaSquarRes.setBounds(44, 50, 147, 52);
		panel_square_res.add(chukaSquarRes);

		JLabel lblValidation2 = new JLabel("");
		lblValidation2.setForeground(new Color(255, 128, 128));
		lblValidation2.setHorizontalAlignment(SwingConstants.CENTER);
		lblValidation2.setVerifyInputWhenFocusTarget(false);
		lblValidation2.setBounds(10, 56, 235, 34);
		panel_chukaSquares.add(lblValidation2);

		size = new JTextField();
		size.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (size.getText().equals("n")) {
					size.setText("");
					size.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (size.getText().isEmpty()) {
					size.setForeground(Color.GRAY);
					size.setText("n");
				}
			}
		});
		size.setText("n");
		size.setHorizontalAlignment(SwingConstants.CENTER);
		size.setColumns(10);
		size.setBounds(10, 11, 47, 34);
		panel_chukaSquares.add(size);

		JButton check2 = new JButton("");
		check2.setIcon(
				new ImageIcon("D:\\apply\\B190910051_week3_problems.zip_expanded\\Problems\\images\\click50px.png"));
		check2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!size.getText().isEmpty() && !fieldArray.getText().isEmpty()) {
					if (isNumeric(size.getText())) {
						int colorSize = Integer.parseInt(size.getText());
						String[] colorsText = fieldArray.getText().split(",");
						if (colorsText.length != colorSize) {
							lblValidation2.setText("Талбарыг гүйцэт бөглөнө үү!");
							return;
						}
						int[] colors = new int[colorSize];
						for (int i = 0; colorsText.length > i; i++) {
							colorsText[i] = colorsText[i].trim();
							if (isNumeric(colorsText[i])) {
								colors[i] = Integer.parseInt(colorsText[i]);
							} else {
								lblValidation2.setText("Форматын дагуу бөглөнө үү!");
								break;
							}

							if (i == colorsText.length - 1) {
								lblValidation2.setText(null);
								chukaSquarRes.setText(Problems.chukaSquares(colorSize, colors));
							}
						}
					} else {
						lblValidation2.setText("Зөвхөн натурал тоо!");
					}
				} else {
					lblValidation2.setText("Талбарыг бөгөлнө үү!");
				}
			}
		});
		check2.setBounds(10, 97, 235, 59);
		panel_chukaSquares.add(check2);

		JScrollPane problame2 = new JScrollPane();
		problame2.setBounds(277, 11, 317, 301);
		panel_chukaSquares.add(problame2);

		JTextArea desc2 = new JTextArea();
		desc2.setLineWrap(true);
		desc2.setWrapStyleWord(true);
		desc2.setText(
				"Чулуунаад ялгаатай өнгийн будагтай n шилэн сав байна. Бүх сав 1-c n хүртэл дугаарлагдсан\r\nба i-р сав ai литр i өнгөтэй будаг агуулсан.\r\nЧулуунаад мөн хязгааргүй урттай 1x1 хэмжээтэй квадратуудаас бүрдсэн 1 өргөнтэй цаас\r\nбайна. Квадратууд 1, 2, 3 гээд цааш дугаарлагдана. Чулуунаа квадратуудыг нэг нэгээр нь\r\nзүүнээс баруун тийш, 1 дугаартай квадратаас эхлээд дурын өнгөөр будаж эхлэхээр шийдсэн.\r\nХэрвээ квадрат x өнгөөр будагдсан байсан бол дараагийн квадрат x+1 өнгөөр будагдсан байх\r\nболно. x=n тохиолдолд дараагийн квадрат 1 дугаарын өнгөөр будагдана. Хэрвээ Чулуунаагийн\r\nбудахыг хүссэн өнгөтэй будаг одоо байхгүй бол тэр зогсоно.\r\nКвадрат үргэлж зөвхөн нэг өнгөөр будагдах ба 1 литр будаг хэрэглэнэ. Таны ажил бол хамгийн\r\nолон нүд будагдахаар Чулуунаагийн эхлэх өнгийг сонгох юм.\r\nОролт:\r\nОролтын эхний мөрөнд нэг ширхэг бүхэл тоон утга n (1≤n≤200000) байх ба Чулуунаад байгаа\r\nөнгөтэй савнуудын тоо байна.\r\nОролтын хоёр дахь мөрөнд бүхэл тоон утгуудын дараалал a1,a2,...,an (1≤ai≤109\r) байх ба энд ai\r\nнь i-р саванд хэдэн литр будаг байгаа тоотой тэнцүү, өөрөөр Чулуунаад байгаа i өнгөтэй\r\nбудагны литрийн тоо гэсэн үг.\r\nГаралт:\r\nГаралтын нэг мөрөнд нэг ширхэг бүхэл тоон утга байх ёстой ба энэ нь хэрвээ Чулуунаа дээр\r\nтодорхойлсон дүрмүүдийг дагасан бол будаж чадах квадратуудын хамгийн их тоо байна.\r\n\r\nЖишээ тэстүүд:\r\nОролт:\r\n5\r\n2 4 2 3 3\r\nГаралт: 12\r\n\r\nОролт:\r\n3\r\n5 5 5\r\nГаралт: 15\r\nОролт:\r\n6\r\n10 10 10 1 10 10\r\n\r\nГаралт: 11\r\n\r\nТэмдэглэл:\r\nЭхний жишээн дээр шилдэг стратеги нь 4 дугаартай өнгийг ашиглан будаж эхлэх юм. Ингээд\r\nквадратууд дараах өнгүүдээр будагдана (зүүнээс баруун): 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5.\r\nХоёр дахь жишээн дээр Чулуунаа дурын өнгөөр будаж эхэлж болно.\r\nГурав дахь жишээн дээр Чулуунаа 5 дугаартай өнгийг ашиглан будаж эхлэх ёстой.");
		problame2.setViewportView(desc2);

		fieldArray = new JTextField();
		fieldArray.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (fieldArray.getText().equals("a1,a2,...,an")) {
					fieldArray.setText("");
					fieldArray.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (fieldArray.getText().isEmpty()) {
					fieldArray.setForeground(Color.GRAY);
					fieldArray.setText("a1,a2,...,an");
				}
			}
		});
		fieldArray.setHorizontalAlignment(SwingConstants.CENTER);
		fieldArray.setText("a1,a2,...,an");
		fieldArray.setBounds(67, 11, 178, 34);
		panel_chukaSquares.add(fieldArray);
		fieldArray.setColumns(10);

		// Problem 3

		JPanel panelSpiralMatrix = new JPanel();
		tabbedPane.addTab("Спирал матриц", null, panelSpiralMatrix, null);
		panelSpiralMatrix.setLayout(null);

		JPanel panel_res3 = new JPanel();
		panel_res3.setLayout(null);
		panel_res3.setBorder(null);
		panel_res3.setBackground(new Color(255, 218, 185));
		panel_res3.setBounds(10, 167, 235, 145);
		panelSpiralMatrix.add(panel_res3);

		JLabel lblres3 = new JLabel("Хариу");
		lblres3.setFont(new Font("Arial", Font.BOLD, 16));
		lblres3.setBounds(93, 11, 105, 28);
		panel_res3.add(lblres3);

		spiralMatrixRes = new JTextField();
		spiralMatrixRes.setHorizontalAlignment(SwingConstants.CENTER);
		spiralMatrixRes.setEditable(false);
		spiralMatrixRes.setColumns(10);
		spiralMatrixRes.setBounds(44, 50, 147, 52);
		panel_res3.add(spiralMatrixRes);

		JLabel lblValidation3 = new JLabel("");
		lblValidation3.setForeground(new Color(255, 128, 128));
		lblValidation3.setHorizontalAlignment(SwingConstants.CENTER);
		lblValidation3.setVerifyInputWhenFocusTarget(false);
		lblValidation3.setBounds(10, 56, 235, 34);
		panelSpiralMatrix.add(lblValidation3);

		spiralMatrixSize = new JTextField();
		spiralMatrixSize.setText("size");
		spiralMatrixSize.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (spiralMatrixSize.getText().equals("size")) {
					spiralMatrixSize.setText("");
					spiralMatrixSize.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (spiralMatrixSize.getText().isEmpty()) {
					spiralMatrixSize.setForeground(Color.GRAY);
					spiralMatrixSize.setText("size");
				}
			}
		});
		spiralMatrixSize.setHorizontalAlignment(SwingConstants.CENTER);
		spiralMatrixSize.setColumns(10);
		spiralMatrixSize.setBounds(10, 11, 109, 34);
		panelSpiralMatrix.add(spiralMatrixSize);

		JButton check3 = new JButton("");
		check3.setIcon(
				new ImageIcon("D:\\apply\\B190910051_week3_problems.zip_expanded\\Problems\\images\\click50px.png"));
		check3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!spiralMatrixSize.getText().isBlank() && !txtX.getText().isBlank() && !txtY.getText().isBlank()) {
					if (isNumeric(spiralMatrixSize.getText()) && isNumeric(txtX.getText())
							&& isNumeric(txtY.getText())) {
						int size = Integer.parseInt(spiralMatrixSize.getText());
						int x = Integer.parseInt(txtX.getText());
						int y = Integer.parseInt(txtY.getText());
						if (size <= x || size <= x) {
							lblValidation3.setText("x,y-г size-с их буюу тэнцүү байна!");
						} else {
							lblValidation3.setText(null);
							spiralMatrixRes.setText(String.valueOf(Problems.spiralValue(size, x, y)));
						}
					} else
						lblValidation3.setText("Зөвхөн натурал тоо !!!");
				} else {
					lblValidation3.setText("Талбарыг бөгөлнө үү!");
				}
			}
		});
		check3.setBounds(10, 97, 235, 59);
		panelSpiralMatrix.add(check3);

		JScrollPane problem3 = new JScrollPane();
		problem3.setBounds(277, 11, 317, 302);
		panelSpiralMatrix.add(problem3);

		JTextArea desc3 = new JTextArea();
		desc3.setText(
				"NxN бүхий спирал матрицын x мөр, y багана дээрх тоог буцаана. Матрицын индекс 0-с\r\nэхлэнэ.\r\n\r\nОролт: n утга (3 <= n <= 1000000).\r\nX,y утга (0<=x,y<n) байна.\r\nГаралт: x, y индекс дээрх утга.\r\n\r\nЖишээ тестүүд:\r\nОролт:\r\n3\r\n2 1\r\nГаралт: 6\r\n\r\nТэмдэглэл:\r\n1 2 3\r\n8 9 4\r\n7 6 5 бүхий спирал матрицын x = 2, y = 1 бол 6 гэж буцаана.\r\nTime Complexity: O(1)");
		desc3.setWrapStyleWord(true);
		desc3.setLineWrap(true);
		problem3.setViewportView(desc3);

		txtX = new JTextField();
		txtX.setHorizontalAlignment(SwingConstants.CENTER);
		txtX.setText("X");
		txtX.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtX.getText().equals("X")) {
					txtX.setText("");
					txtX.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (txtX.getText().isEmpty()) {
					txtX.setForeground(Color.GRAY);
					txtX.setText("X");
				}
			}
		});
		txtX.setBounds(135, 11, 50, 34);
		panelSpiralMatrix.add(txtX);
		txtX.setColumns(10);

		txtY = new JTextField();
		txtY.setText("Y");
		txtY.setHorizontalAlignment(SwingConstants.CENTER);
		txtY.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtY.getText().equals("Y")) {
					txtY.setText("");
					txtY.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (txtY.getText().isEmpty()) {
					txtY.setForeground(Color.GRAY);
					txtY.setText("Y");
				}
			}
		});
		txtY.setColumns(10);
		txtY.setBounds(195, 11, 50, 34);
		panelSpiralMatrix.add(txtY);

	}
}
